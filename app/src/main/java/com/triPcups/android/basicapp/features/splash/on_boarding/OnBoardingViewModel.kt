package com.triPcups.android.basicapp.features.splash.on_boarding

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.triPcups.android.basicapp.common.ProgressData
import com.triPcups.android.basicapp.local_db.AppSettings

class OnBoardingViewModel(private val appSettings: AppSettings) : ViewModel() {

    enum class NavigationEvent {
        GO_TO_HOME,
        GO_TO_SIGN_IN
    }

    val progressData = ProgressData()
    val navEvent = MutableLiveData<NavigationEvent>()


    fun onFinish(){
        progressData.startProgress()

        appSettings.setFirstTimeInApp()
        navEvent.postValue(NavigationEvent.GO_TO_HOME)

        progressData.endProgress()
    }

}
