package com.triPcups.android.basicapp.features.auth

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.triPcups.android.basicapp.R
import com.triPcups.android.basicapp.features.auth.signin.SignInFragment

class AuthActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.auth_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, SignInFragment.newInstance())
                .commitNow()
        }
    }

}
