package com.triPcups.android.basicapp.custom_views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import com.triPcups.android.basicapp.R
import kotlinx.android.synthetic.main.progress_bar.view.*


class ProgressBar : FrameLayout {

    interface ProgressBarListener {
        fun onLoading(isLoading: Boolean) {}
    }

    companion object {
        //Progress Type values
        const val PROGRESS_BAR_SYSTEM_TYPE: Int = 0
        const val PROGRESS_BAR_ANIMATION_TYPE: Int = 1

        // Progress Animation values
        const val PROGRESS_BAR_DEFAULT_ANIMATION: Int = 0
        const val PROGRESS_BAR_SECONDARY_ANIMATION: Int = 1
        const val PROGRESS_BAR_THIRD_ANIMATION: Int = 2
    }

    private var viewType: Int = 0
    var listener: ProgressBarListener? = null

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context,attrs,defStyleAttr) {
        LayoutInflater.from(context).inflate(R.layout.progress_bar,this,true)

        if (attrs != null) {
            val a = context.obtainStyledAttributes(attrs, R.styleable.ProgressBarAttrs)
                val isLoading = a.getBoolean(R.styleable.ProgressBarAttrs_isLoading,false)
                showProgressBar(isLoading)

                val type = a.getInt(R.styleable.ProgressBarAttrs_progressType,PROGRESS_BAR_SYSTEM_TYPE)
                initUi(type)

                val animType = a.getInt(R.styleable.ProgressBarAttrs_progressAnimation,PROGRESS_BAR_DEFAULT_ANIMATION)
                initAnimationStyle(animType)

                val isBlocking = a.getBoolean(R.styleable.ProgressBarAttrs_isClickBlocking,true)
                setClickBlocking(isBlocking)
            a.recycle()
        }
    }

    fun setClickBlocking(blocking: Boolean) {
        if(blocking){
            progressBarLayout.isClickable = true
            progressBarLayout.isFocusable = true
        } else {
            progressBarLayout.isClickable = false
            progressBarLayout.isFocusable = false
        }
        invalidate()
    }

    fun initAnimationStyle(type: Int) {
        when(type) {
            PROGRESS_BAR_DEFAULT_ANIMATION -> {
                setLoadingAnimationResId(R.raw.menu_anim)
            }
            PROGRESS_BAR_SECONDARY_ANIMATION -> {
                setLoadingAnimationResId(R.raw.menu_anim)
            }
            PROGRESS_BAR_THIRD_ANIMATION -> {
                setLoadingAnimationResId(R.raw.menu_anim)
            }
        }
    }

    fun showProgressBar(loading: Boolean) {
        listener?.onLoading(loading)
        playLoadingAnimation(loading && viewType == PROGRESS_BAR_ANIMATION_TYPE)

        if(loading){
            progressBarLayout.visibility = View.VISIBLE
        } else {
            progressBarLayout.visibility = View.GONE
        }
    }

    private fun initUi(type: Int) {
        this.viewType = type
        hideAll()
        when (type) {
            PROGRESS_BAR_SYSTEM_TYPE -> {
                progressBarPb.visibility = View.VISIBLE
            }
            PROGRESS_BAR_ANIMATION_TYPE -> {
                progressBarAnimation.visibility = View.VISIBLE
            }
        }
    }

    private fun hideAll() {
        progressBarPb.visibility = View.GONE
        progressBarAnimation.visibility = View.GONE
    }

    private fun playLoadingAnimation(show: Boolean) {
        if(show){
            if (progressBarAnimation.isAnimating) {
                progressBarAnimation.resumeAnimation() //TODO:: test this
            }else {
                progressBarAnimation.playAnimation()
            }
        } else {
            progressBarAnimation.pauseAnimation()
        }
    }

    private fun setLoadingAnimationResId(resId: Int) {
        progressBarAnimation.setAnimation(resId)
    }

}