package com.triPcups.android.basicapp.features.splash.on_boarding

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.hardik.clickshrinkeffect.applyClickShrink
import com.triPcups.android.basicapp.R
import com.triPcups.android.basicapp.common.OnSingleClickListener
import kotlinx.android.synthetic.main.on_boarding_screen.view.*


class OnBoardingScreen : LinearLayout {


    interface OnBoardingScreenListener {
        fun showNextOnBoardingScreen() {}
    }

    var listener: OnBoardingScreenListener? = null

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        LayoutInflater.from(context).inflate(
            R.layout.on_boarding_screen,
            this,
            true
        )

        if (attrs != null) {
            val a = context.obtainStyledAttributes(attrs, R.styleable.OnBoardingScreenAttrs)
            if (a.hasValue(R.styleable.OnBoardingScreenAttrs_title)) {
                val title = a.getString(R.styleable.OnBoardingScreenAttrs_title)
                setTitle(title)
            }
            if (a.hasValue(R.styleable.OnBoardingScreenAttrs_text)) {
                val text = a.getString(R.styleable.OnBoardingScreenAttrs_text)
                setText(text)
            }
            if (a.hasValue(R.styleable.OnBoardingScreenAttrs_image)) {
                val image = a.getDrawable(R.styleable.OnBoardingScreenAttrs_image)
                setImage(image)
            }
            if (a.hasValue(R.styleable.OnBoardingScreenAttrs_isButtonVisible)) {
                val isVisible = a.getBoolean(R.styleable.OnBoardingScreenAttrs_isButtonVisible, false)
                setButtonVisibility(isVisible)
            }
            if(a.hasValue(R.styleable.OnBoardingScreenAttrs_isFirst)){
                val isFirst = a.getBoolean(R.styleable.OnBoardingScreenAttrs_isFirst, false)
                setIsFirst(isFirst)
            }
            if(a.hasValue(R.styleable.OnBoardingScreenAttrs_isLast)){
                val isLast = a.getBoolean(R.styleable.OnBoardingScreenAttrs_isLast, false)
                setIsLast(isLast)
            }
            a.recycle()
        }

        initClicks()
    }

    private fun setIsLast(isLast: Boolean) {
        if(isLast) {
            onBoardingScreenPrevBtn.visibility = View.VISIBLE
            onBoardingScreenNextBtn.visibility = View.GONE
            onBoardingScreenPrevBtn.isEnabled = false
        } else {
            onBoardingScreenNextBtn.visibility = View.VISIBLE
        }
    }

    private fun setIsFirst(isFirst: Boolean) {
        onBoardingScreenNextBtn.visibility = View.VISIBLE
        if(isFirst) {
            onBoardingScreenPrevBtn.visibility = View.GONE
        } else {
            onBoardingScreenPrevBtn.visibility = View.VISIBLE
        }
    }

    private fun setButtonVisibility(visible: Boolean) {
        if(visible){
            onBoardingScreenButton.visibility = View.VISIBLE
        } else {
            onBoardingScreenButton.visibility = View.GONE
        }
    }

    private fun setText(text: String?) {
        if(!text.isNullOrEmpty()){
            onBoardingScreenText.text = text
        } else {
            onBoardingScreenText.setText(R.string.app_name)
        }
    }

    fun setImage(image: Drawable?) {
        onBoardingScreenImage.setImageDrawable(image)
    }

    private fun initClicks() {
        onBoardingScreenButton.applyClickShrink()

//        onBoardingScreenLayout.setOnClickListener(object : OnSingleClickListener() {
//            override fun onSingleClick(v: View?) {
//                listener?.showNextOnBoardingScreen()
//            }
//        })
        onBoardingScreenButton.setOnClickListener(object : OnSingleClickListener() {
            override fun onSingleClick(v: View?) {
                listener?.showNextOnBoardingScreen()
            }
        })
    }

    fun setTitle(screenName: String?) {
        onBoardingScreenTitle.text = screenName
    }

}