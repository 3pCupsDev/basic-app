package com.triPcups.android.basicapp.dependency_injection

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.triPcups.android.basicapp.local_db.AppSettings
import com.triPcups.android.basicapp.local_db.Constants
import com.triPcups.android.basicapp.networking.ApiService
import com.yariksoffice.lingver.Lingver
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {

    //LocalDb
    single { provideSharedPreferences(get()) }
    factory { AppSettings(get()) }

    //networking
    single { provideDefaultOkHttpClient() }
    single { provideRetrofit(get()) }
    single { provideApiService(get()) }

    //Languager
    factory { getLingver() }

    //Firebase Services
//    single { provideFireStore() }
//    single { provideFirebaseDatabase() }
//    single { provideFirebaseAnalytics(get()) }
//    single { provideFirebaseRemoteConfig() }
}


fun provideApiService(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)

//fun provideFireStore(): FirebaseFirestore {
//    return FirebaseFirestore.getInstance()
//}

//fun provideFirebaseDatabase(): FirebaseDatabase {
//    return FirebaseDatabase.getInstance()
//}

//fun provideFirebaseAnalytics(context: Context): FirebaseAnalytics {
//    return FirebaseAnalytics.getInstance(context)
//}

//fun provideFirebaseRemoteConfig(): FirebaseRemoteConfig {
//    return FirebaseRemoteConfig.getInstance()
//}


fun getLingver(): Lingver {
    return Lingver.getInstance()
}


fun provideSharedPreferences(context: Context): SharedPreferences {
    return context.getSharedPreferences("User", Context.MODE_PRIVATE)
}

fun provideDefaultOkHttpClient(): OkHttpClient {
    val logging = HttpLoggingInterceptor()
    logging.level = HttpLoggingInterceptor.Level.BODY

    val httpClient = OkHttpClient.Builder().addInterceptor(logging)
    return httpClient.build()
}

fun provideRetrofit(client: OkHttpClient): Retrofit {
    val gson = GsonBuilder()
        .setDateFormat("yyyy-MM-dd HH:mm:ss Z")
        .create()

    return Retrofit.Builder()
        .baseUrl(Constants.BASE_SERVER_URL)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()
}
