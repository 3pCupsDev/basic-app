package com.triPcups.android.basicapp.custom_views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import com.triPcups.android.basicapp.R
import com.triPcups.android.basicapp.common.OnSingleClickListener
import kotlinx.android.synthetic.main.bottom_bar.view.*


class BottomBar : FrameLayout {

    interface BottomBarListener {
        fun onBottomProfileClick() {}
        fun onBottomMenuClick() {}
        fun onBottomHomeClick() {}
        fun onBottomUpdateClick() {}
        fun onBottomCartClick() {}
    }

    var listener: BottomBarListener? = null

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        LayoutInflater.from(context).inflate(
            R.layout.bottom_bar,
            this,
            true
        )

//        if (attrs != null) {
////            val a = context.obtainStyledAttributes(attrs, R.styleable.BottomBar)
////            if (a.hasValue(R.styleable.HeaderViewAttrs_title)) {
////                var title = a.getString(R.styleable.HeaderViewAttrs_title)
////                headerViewTitle.text = title
////            }
////            if (a.hasValue(R.styleable.HeaderViewAttrs_type)) {
////                var type = a.getInt(
////                    R.styleable.HeaderViewAttrs_type,
////                    HEADER_VIEW_TYPE_TITLE_MENU_SEARCH
////                )
////                initUi(type)
////            }
////            a.recycle()
//        }

        initClicks()
    }


//    public fun setType(type: Int?, title: String?) {
//        setTitle(title)
//        initUi(type)
//    }


    private fun initClicks() {
        bottomBarProfileBtn.setOnClickListener(object : OnSingleClickListener() {
            override fun onSingleClick(v: View?) {
                unselectAll()
                bottomBarProfileBtn.isSelected = true
                listener?.onBottomProfileClick()
            }
        })
        bottomBarMenuBtn.setOnClickListener(object : OnSingleClickListener() {
            override fun onSingleClick(v: View?) {
                unselectAll()
                bottomBarMenuBtn.isSelected = true
                listener?.onBottomMenuClick()
            }
        })
        bottomBarHomeBtn.setOnClickListener(object : OnSingleClickListener() {
            override fun onSingleClick(v: View?) {
                unselectAll()
                bottomBarHomeBtn.isSelected = true
                listener?.onBottomHomeClick()
            }
        })
        bottomBarUpdatesBtn.setOnClickListener(object : OnSingleClickListener() {
            override fun onSingleClick(v: View?) {
                unselectAll()
                bottomBarUpdatesBtn.isSelected = true
                listener?.onBottomUpdateClick()
            }
        })
        bottomBarCartBtn.setOnClickListener(object : OnSingleClickListener() {
            override fun onSingleClick(v: View?) {
                unselectAll()
                bottomBarCartBtn.isSelected = true
                listener?.onBottomCartClick()
            }
        })
    }

    private fun unselectAll() {
        bottomBarProfileBtn.isSelected = false
        bottomBarMenuBtn.isSelected = false
        bottomBarHomeBtn.isSelected = false
        bottomBarUpdatesBtn.isSelected = false
        bottomBarCartBtn.isSelected = false
    }

//    private fun initUi(type: Int?) {
//        searchBtn.applyClickShrink()
//        menuBtn.applyClickShrink()
//        saveBtn.applyClickShrink()
//
//        hideAll()
//        when (type) {
//            HEADER_VIEW_TYPE_TITLE_MENU -> {
//                menuBtn.visibility = View.VISIBLE
//            }
//            HEADER_VIEW_TYPE_TITLE_MENU_SEARCH -> {
//                menuBtn.visibility = View.VISIBLE
//                searchBtn.visibility = View.VISIBLE
//            }
//            HEADER_VIEW_TYPE_TITLE_MENU_NEXT -> {
//                menuBtn.visibility = View.VISIBLE
//                saveBtn.visibility = View.VISIBLE
//            }
////            HEADER_VIEW_TYPE_TITLE_CLOSE_DONE -> {
////                menuBtn.visibility = View.VISIBLE
////                closeBtn.visibility = View.VISIBLE
////            }
//        }
//    }

//    private fun hideAll() {
//        searchBtn.visibility = View.GONE
//        menuBtn.visibility = View.GONE
//        saveBtn.visibility = View.GONE
////        closeBtn.visibility = View.GONE
//    }
//
//    fun playMenuAnimation(isOpen: Boolean) {
//        if (isOpen) {
//            menuBtn.playAnimation()
//
//            mHandler.postDelayed({
//                menuBtn?.apply {
//                    pauseAnimation()
//                    progress = 0.47f
//                }
//            }, Constants.SMALL_DELAY_MS * 4)
//        } else {
//            menuBtn.resumeAnimation()
//        }
//    }
//
//    fun setTitle(screenName: String?) {
//        headerViewTitle.text = screenName
//    }


}