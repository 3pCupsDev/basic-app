package com.triPcups.android.basicapp.networking

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET

interface ApiService {

    @GET("users/")
    fun getUsers(): Call<ResponseBody>
}