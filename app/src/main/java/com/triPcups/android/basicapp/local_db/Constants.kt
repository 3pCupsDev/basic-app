package com.triPcups.android.basicapp.local_db

class Constants {

    companion object{
        //MainAct Params
        const val RATE_US_DIALOG_MIN_SESSION_TO_APPEAR: Int = 7
        const val RATE_US_DIALOG_RATING_THRESHOLD: Float = 4f

        //Fragment's Tags
        const val SPLASH_LOGO_FRAGMENT_TAG = "SPLASH_LOGO_FRAGMENT_TAG"
        const val ON_BOARDING_FRAGMENT_TAG = "ON_BOARDING_FRAGMENT_TAG"
        const val SIGN_IN_FRAGMENT_TAG = "SIGN_IN_FRAGMENT_TAG"
        const val MAIN_FRAGMENT_TAG = "MAIN_FRAGMENT_TAG"


        //Networking
        const val APP_WEB_HOME_PAGE_URL: String ="https://3p-cups.com"
        const val BASE_SERVER_URL: String = "https://base.server.url/"
        const val GOOGLE_STORE_BASE_URL = "https://play.google.com/store/apps/details?id="


        //Delays in Ms
        const val SMALL_DELAY_MS: Long = 200
        const val MEDIUM_DELAY_MS: Long = 400
        const val ONE_SECOND_DELAY_MS: Long = 1000
        const val MORE_THAN_ONE_SEC_DELAY_MS: Long = 1800
        const val MAX_LOADING_TIME_MS: Long = 13000
        const val REFRESH_DATA_RATE_MS: Long = 19000
        const val CACHE_EXPIRATION: Long = 960000

        //About Config Params
        const val ABOUT_CONFIG_DEVELOPER_EMAIL_ADDRESS: String = "support@3p-cups.com"
        const val ABOUT_CONFIG_EMAIL_SUBJECT_TEMPLATE: String = "I want Support for "
        const val ABOUT_CONFIG_EMAIL_BODY_TEMPLATE: String = "A few notes i have to say:\n"

        const val ABOUT_CONFIG_SHARE_ACTION_TEMPLATE: String = "Check out this Cool App!\n"

    }
}