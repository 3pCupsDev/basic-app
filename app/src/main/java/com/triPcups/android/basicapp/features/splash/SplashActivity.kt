package com.triPcups.android.basicapp.features.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.prathameshmore.toastylibrary.Toasty
import com.triPcups.android.basicapp.R
import com.triPcups.android.basicapp.common.AndroidBarsManager
import com.triPcups.android.basicapp.features.MainActivity
import com.triPcups.android.basicapp.features.auth.signin.SignInFragment
import com.triPcups.android.basicapp.features.splash.on_boarding.OnBoardingFragment
import com.triPcups.android.basicapp.features.splash.splash_logo.SplashLogoFragment
import com.triPcups.android.basicapp.local_db.Constants
import com.triPcups.android.basicapp.models.MessageType
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity(), SplashLogoFragment.SplashLogoFragmentListener,
    OnBoardingFragment.OnBoardingFragmentListener {

    private lateinit var toasty: Toasty
    private val mHandler =  Handler(android.os.Looper.getMainLooper())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(FLAG_FULLSCREEN,FLAG_FULLSCREEN)
        AndroidBarsManager.setColor(window,getColor(R.color.colorPrimary),getColor(R.color.colorPrimary))
        setContentView(R.layout.activity_splash)

        initUtils()
        initSplash()
    }

    private fun initUtils() {
        toasty = Toasty(this@SplashActivity)

    }

    fun showSplashLogoFragment(){
        showFragment(SplashLogoFragment.newInstance(),Constants.SPLASH_LOGO_FRAGMENT_TAG)
    }

    fun showOnBoardingFragment(){
        showFragment(OnBoardingFragment.newInstance(),Constants.ON_BOARDING_FRAGMENT_TAG)
    }


    private fun showFragment(fragment: Fragment, tag: String) {
        supportFragmentManager.beginTransaction()
            .setCustomAnimations(
                R.anim.anim_in, 0, 0,
                R.anim.anim_out
            )
            .replace(R.id.splashActContainer, fragment,tag)
            .commit()
    }


    private fun initSplash() {
        showSplashLogoFragment()
    }

    override fun showMessage(msg: String?, messageType: MessageType) {
        msg?.let{
            when(messageType) {
                MessageType.FAILURE ->{
                    toasty.dangerToasty(
                        this,
                        msg,
                        Toasty.LENGTH_LONG,
                        Toasty.BOTTOM
                    )
                }
                MessageType.CAUTION ->{
                    toasty.warningToasty(
                        this,
                        msg,
                        Toasty.LENGTH_LONG,
                        Toasty.BOTTOM
                    )
                }
                MessageType.SUCCESS ->{
                    toasty.successToasty(
                        this,
                        msg,
                        Toasty.LENGTH_LONG,
                        Toasty.BOTTOM
                    )
                }
                MessageType.NEAUTRAL ->{
                    toasty.infoToasty(
                        this,
                        msg,
                        Toasty.LENGTH_LONG,
                        Toasty.BOTTOM
                    )
                }
            }
        }
    }

    override fun startNextActivity() {
        mHandler.postDelayed({
            startActivity(Intent(this,MainActivity::class.java))
            overridePendingTransition(R.anim.anim_in,R.anim.anim_out)
            finish()

        }, Constants.MORE_THAN_ONE_SEC_DELAY_MS)
    }

    override fun showSignInDialog(){
        mHandler.postDelayed({
            onLoading(true)
            mHandler.postDelayed({
                onLoading(false)

                showSignInFragment()
            }, Constants.MORE_THAN_ONE_SEC_DELAY_MS)
        }, Constants.ONE_SECOND_DELAY_MS)
    }

    private fun showSignInFragment() {
        showFragment(SignInFragment.newInstance(),Constants.SIGN_IN_FRAGMENT_TAG)
        showMessage("Redirected to Sign In", MessageType.NEAUTRAL)
    }

    override fun showFirstTimeInAppDialog() {
        mHandler.postDelayed({
            onLoading(true)
            mHandler.postDelayed({
                onLoading(false)

                showOnBoardingFragment()
            }, Constants.MORE_THAN_ONE_SEC_DELAY_MS)
        }, Constants.ONE_SECOND_DELAY_MS)
    }

    override fun onLoading(isLoading: Boolean){
        splashActPb.showProgressBar(isLoading)
    }
}
