package com.triPcups.android.basicapp.networking

import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.triPcups.android.basicapp.R
import kotlinx.android.synthetic.main.app_bar_drawer.*
import java.lang.Exception

open class NetworkActivity : AppCompatActivity(),
    NetworkChangeReceiver.ConnectivityReceiverListener {

    protected var snackBar: Snackbar? = null


    protected var receiver: NetworkChangeReceiver? = null

    private fun initNetworkChangedReceiver() {
        receiver = NetworkChangeReceiver()
        registerReceiver(receiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }


    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        initNetworkChangedReceiver()
    }

    override fun onDestroy() {
        try{
            receiver?.let{
                unregisterReceiver(it)
            }
        } catch (e: Exception) {}
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        NetworkChangeReceiver.connectivityReceiverListener = this
    }

    private fun showNetworkMessage(isConnected: Boolean) {
        if (!isConnected) {
            mainActContainer?.let {
                snackBar = Snackbar.make(mainActContainer, getString(R.string.u_r_offline), BaseTransientBottomBar.LENGTH_INDEFINITE)
            }
            snackBar?.show()
        } else {
//            Toast.makeText(this, getString(R.string.u_r_online), Toast.LENGTH_LONG).show()
            snackBar?.dismiss()
        }
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showNetworkMessage(isConnected)
    }

}