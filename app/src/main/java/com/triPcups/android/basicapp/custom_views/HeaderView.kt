package com.triPcups.android.basicapp.custom_views

import android.content.Context
import android.os.Handler
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import com.hardik.clickshrinkeffect.applyClickShrink
import com.triPcups.android.basicapp.R
import com.triPcups.android.basicapp.local_db.Constants
import kotlinx.android.synthetic.main.header_view.view.*


class HeaderView : FrameLayout {

    interface HeaderViewListener {
        fun onHeaderMenuClick() {}
        fun onHeaderSearchClick(screenName: String) {}
        fun onHeaderCloseClick() {}
    }


    companion object {
        const val HEADER_VIEW_TYPE_TITLE_MENU_SEARCH: Int = 0
        const val HEADER_VIEW_TYPE_TITLE_MENU: Int = 1
        const val HEADER_VIEW_TYPE_TITLE_MENU_NEXT: Int = 2
    }

    private val mHandler =  Handler(android.os.Looper.getMainLooper())
    var listener: HeaderViewListener? = null

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        LayoutInflater.from(context).inflate(
            R.layout.header_view,
            this,
            true
        )

        if (attrs != null) {
            val a = context.obtainStyledAttributes(attrs, R.styleable.HeaderViewAttrs)
            if (a.hasValue(R.styleable.HeaderViewAttrs_title)) {
                var title = a.getString(R.styleable.HeaderViewAttrs_title)
                headerViewTitle.text = title
            }
            if (a.hasValue(R.styleable.HeaderViewAttrs_type)) {
                var type = a.getInt(
                    R.styleable.HeaderViewAttrs_type,
                    HEADER_VIEW_TYPE_TITLE_MENU_SEARCH
                )
                initUi(type)
            }
            a.recycle()
        }

        initClicks()
    }


    public fun setType(type: Int?, title: String?) {
        setTitle(title)
        initUi(type)
    }

    private fun initClicks() {
        menuBtn.setOnClickListener {
//            playMenuAnimation(true)
            listener?.onHeaderMenuClick()
        }
        searchBtn.setOnClickListener {
            listener?.onHeaderSearchClick(headerViewTitle.text.toString())
        }
//        headerViewNextBtn.setOnClickListener {
//            listener?.onHeaderCloseClick()
//        }
    }

    private fun initUi(type: Int?) {
        searchBtn.applyClickShrink()
        menuBtn.applyClickShrink()
        saveBtn.applyClickShrink()

        hideAll()
        when (type) {
            HEADER_VIEW_TYPE_TITLE_MENU -> {
                menuBtn.visibility = View.VISIBLE
            }
            HEADER_VIEW_TYPE_TITLE_MENU_SEARCH -> {
                menuBtn.visibility = View.VISIBLE
                searchBtn.visibility = View.VISIBLE
            }
            HEADER_VIEW_TYPE_TITLE_MENU_NEXT -> {
                menuBtn.visibility = View.VISIBLE
                saveBtn.visibility = View.VISIBLE
            }
//            HEADER_VIEW_TYPE_TITLE_CLOSE_DONE -> {
//                menuBtn.visibility = View.VISIBLE
//                closeBtn.visibility = View.VISIBLE
//            }
        }
    }

    private fun hideAll() {
        searchBtn.visibility = View.GONE
        menuBtn.visibility = View.GONE
        saveBtn.visibility = View.GONE
//        closeBtn.visibility = View.GONE
    }

    fun playMenuAnimation(isOpen: Boolean) {
        if (isOpen) {
            menuBtn.setMinAndMaxFrame(0,35)
            menuBtn.playAnimation()
        } else {
            menuBtn.setMinAndMaxFrame(35,90)
            menuBtn.playAnimation()
        }
    }

    fun setTitle(screenName: String?) {
        headerViewTitle.text = screenName
    }


}