package com.triPcups.android.basicapp.common

import android.content.Context
import android.content.res.Configuration
import android.view.Window
import android.view.WindowManager


class AndroidBarsManager {

    companion object {

        fun setColor(window: Window, color: Int) {
            setTopColor(window, color)
            setBottomColor(window, color)
        }

        fun setColor(window: Window, topColor: Int, bottomColor: Int) {
            setTopColor(window, topColor)
            setBottomColor(window, bottomColor)
        }

        fun setTopColor(window: Window, color: Int) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = color
        }

        fun setBottomColor(window: Window, color: Int) {
            window.setFlags(
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
            )
            window.navigationBarColor = color
        }

        fun getNavigationBarHeight(context: Context): Int {
            val resources = context.resources
            val resourceId = resources.getIdentifier(
                if (isPortrait(context)) "navigation_bar_height" else "navigation_bar_height_landscape",
                "dimen",
                "android"
            )
            return if (resourceId > 0) {
                resources.getDimensionPixelSize(resourceId)
            } else 0
        }

        fun getStatusBarHeight(context: Context): Int {
            val resourceId = context.resources.getIdentifier(
                if (isPortrait(context)) "status_bar_height" else "status_bar_height_landscape",
                "dimen",
                "android"
            )
            return if (resourceId > 0) {
                context.resources.getDimensionPixelSize(resourceId)
            } else 0
        }

        fun isPortrait(context: Context): Boolean {
            return (context.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT)
        }
    }
}