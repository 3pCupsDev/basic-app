package com.triPcups.android.basicapp.networking

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager

class NetworkChangeReceiver : BroadcastReceiver() {

    var isConnected: Boolean? = null

    interface ConnectivityReceiverListener {
        fun onNetworkConnectionChanged(isConnected: Boolean)
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        context?.let{
            isConnected = isConnectedOrConnecting(it)
            isConnected?.let{ connected ->
                connectivityReceiverListener?.onNetworkConnectionChanged(connected)
            }
        }
    }

    private fun isConnectedOrConnecting(context: Context): Boolean {
        val connMgr = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connMgr.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnectedOrConnecting
    }



    companion object {
        var connectivityReceiverListener: ConnectivityReceiverListener? = null
    }
}