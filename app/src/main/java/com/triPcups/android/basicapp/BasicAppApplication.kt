package com.triPcups.android.basicapp

import android.app.Application
import android.content.res.Configuration
import com.triPcups.android.basicapp.dependency_injection.appModule
import com.triPcups.android.basicapp.dependency_injection.networkModule
import com.yariksoffice.lingver.Lingver
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class BasicAppApplication : Application() {

    override fun onCreate() {
        super.onCreate()

//        MobileAds.initialize(this, Constants.AD_MOB_APP_ID)

        setupKoin()

        setupLanguage()
    }

    private fun setupLanguage() {
        Lingver.init(this)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        Lingver.getInstance().setLocale(this,newConfig.locales[0])
        super.onConfigurationChanged(newConfig)
    }

    private fun setupKoin() {
        startKoin {
            androidContext(this@BasicAppApplication)
            androidLogger()
            modules(appModule, networkModule)
        }
    }

}