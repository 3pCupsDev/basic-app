package com.triPcups.android.basicapp.common

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat.startActivity
import com.triPcups.android.basicapp.R
import com.triPcups.android.basicapp.local_db.Constants
import org.json.JSONObject
import p32929.updaterlib.AppUpdater
import p32929.updaterlib.UpdateListener
import p32929.updaterlib.UpdateModel

class VersionUpdater {

    companion object {
        fun validateVersion(ctx: Context?, latestVersionJsonUrl: String?) {
            latestVersionJsonUrl?.let {
                ctx?.let {
                    AppUpdater(ctx, latestVersionJsonUrl, object : UpdateListener {
                        override fun onJsonDataReceived(
                            updateModel: UpdateModel,
                            jsonObject: JSONObject
                        ) {
                            if (AppUpdater.getCurrentVersionCode(ctx) < updateModel.versionCode) {
                                openUpdateAvailableDialogAndRevalidate(
                                    ctx,
                                    updateModel.isCancellable,
                                    latestVersionJsonUrl
                                )//,updateModel.url)
                            }
                        }

                        override fun onError(error: String) {}
                    }).execute()
                }
            }
        }

        private fun openUpdateAvailableDialogAndRevalidate(
            ctx: Context?,
            isCancellable: Boolean,
            latestVersionJsonUrl: String
        ) {
            ctx?.let {
                AlertDialog.Builder(it)
                    .setTitle(it.getString(R.string.update))
                    .setMessage(it.getString(R.string.update_avialable))
                    .setIcon(R.drawable.ic_update)
                    .setCancelable(isCancellable)
                    .setPositiveButton(it.getString(R.string.update)) { dialog, which ->
                        val browserIntent = Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse(Constants.GOOGLE_STORE_BASE_URL + it.applicationContext.packageName)
                        )
                        startActivity(ctx, browserIntent, null)

                        //for more strict validation -> uncomment next lines
//                        validateVersion(ctx,latestVersionJsonUrl)
//                        (ctx as Activity).finish()
                    }
                    .show()
            }
        }

        fun getBuildVersion(context: Context): String {
            return try {
                val pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
                pInfo.versionName
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
                "420"
            }

        }
    }
}