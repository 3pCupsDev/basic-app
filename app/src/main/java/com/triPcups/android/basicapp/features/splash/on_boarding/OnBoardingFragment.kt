package com.triPcups.android.basicapp.features.splash.on_boarding

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.core.view.children
import androidx.lifecycle.Observer

import com.triPcups.android.basicapp.R
import com.triPcups.android.basicapp.common.AndroidBarsManager
import com.triPcups.android.basicapp.common.OnSingleClickListener
import com.triPcups.android.basicapp.models.MessageType
import kotlinx.android.synthetic.main.on_boarding_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class OnBoardingFragment : Fragment(), OnBoardingScreen.OnBoardingScreenListener {

    interface OnBoardingFragmentListener{
        fun startNextActivity()
        fun onLoading(isLoading: Boolean)
        fun showMessage(msg: String?, messageType: MessageType)
    }

    companion object {
        fun newInstance() = OnBoardingFragment()
    }

    private var listener: OnBoardingFragmentListener? = null
    private val viewModel by viewModel<OnBoardingViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.on_boarding_fragment, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnBoardingFragmentListener) {
            this.listener = context
        } else {
            throw RuntimeException("$context must implement OnBoardingFragmentListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initObservers()
        initClicks()
        initUi()
    }

    private fun initObservers() {
        viewModel.progressData.observe(viewLifecycleOwner, Observer {
            isLoading -> handleLoading(isLoading) })
        viewModel.navEvent.observe(viewLifecycleOwner, Observer {
            navEvent -> handleNavigationEvents(navEvent) })
    }

    private fun handleNavigationEvents(navEvent: OnBoardingViewModel.NavigationEvent?) {
        navEvent?.let{
            when(it){
                OnBoardingViewModel.NavigationEvent.GO_TO_HOME -> {
                    listener?.startNextActivity()
                }
                OnBoardingViewModel.NavigationEvent.GO_TO_SIGN_IN -> {
                    listener?.showMessage("Go To sign in need implementation", MessageType.CAUTION)
                }
            }
        }
    }

    private fun handleLoading(isLoading: Boolean?) {
        isLoading?.let{
            listener?.onLoading(it)
        }
    }

    private fun initClicks() {
        onBoardingFragNextPage.setOnClickListener(object : OnSingleClickListener(){
            override fun onSingleClick(v: View?) {
                showNextOnBoardingScreen()
            }
        })

        onBoardingFragPrevPage.setOnClickListener(object : OnSingleClickListener(){
            override fun onSingleClick(v: View?) {
                if(onBoardingFragmentViewFlipper.displayedChild > 0){
                    onBoardingFragmentViewFlipper.showPrevious()
                }
            }
        })
    }

    private fun initUi() {
        context?.let{
            val navBarHeight = AndroidBarsManager.getNavigationBarHeight(it)
            onBoardingFragLayout.setPadding(0,0,0,navBarHeight)
        }

        onBoardingFragmentViewFlipper.inAnimation =  AnimationUtils.loadAnimation(context,R.anim.slide_in_bottom)
        onBoardingFragmentViewFlipper.outAnimation =  AnimationUtils.loadAnimation(context,R.anim.slide_out_top)

        for(onBoardingScreen in onBoardingFragmentViewFlipper.children){
            if(onBoardingScreen is OnBoardingScreen) {
                onBoardingScreen.listener = this
            }
        }

//        onBoardingFragmentViewFlipper.setOnDragListener { v, event ->
//            showNextOnBoardingScreen() // TODO:: test this later
//            false
//        }
    }

    override fun showNextOnBoardingScreen() {
        enableControls(true)

        if(onBoardingFragmentViewFlipper.displayedChild == onBoardingFragmentViewFlipper.childCount-1){
            viewModel.onFinish()
        } else {
            if(onBoardingFragmentViewFlipper.displayedChild == onBoardingFragmentViewFlipper.childCount-2){
                enableControls(false)
            }
            onBoardingFragmentViewFlipper.showNext()
        }
    }

    private fun enableControls(isEnabled: Boolean) {
        if(isEnabled){
            onBoardingFragControlsLayout.visibility = View.VISIBLE
        } else {
            onBoardingFragControlsLayout.visibility = View.GONE
        }
    }
}
