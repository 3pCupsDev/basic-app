package com.triPcups.android.basicapp.models

import java.io.Serializable

enum class ErrorEvent {
    NO_ERROR,
    UNSUCCESSFUL_RESPONSE,
    NETWORK_ERROR,
    FAILURE
}

enum class MessageType {
    SUCCESS,
    CAUTION,
    FAILURE,
    NEAUTRAL
}

sealed class SplashNavigationEvent {
    //    class UserSignedIn(val userId: String) : SplashNavigationEvent()
    class UserNeedSignedIn : SplashNavigationEvent()
    class ShowOnBoarding : SplashNavigationEvent()
    class LoadHome : SplashNavigationEvent()
}

sealed class ErrorData {
    class RssFeedNotLoaded(val msg: String?) : ErrorData()
}

class DeviceDetails : Serializable {
    var language: String? = null
    var country: String? = null

    var manufacturer: String? = null

    var osVersion: String? = ""
    var appVersion: String? = ""
    var versionSdk: String? = ""
    var device: String? = ""
    var model: String? = ""
    var product: String? = ""

    var innerMacAddress: String? = null
    var outerMacAddress: String? = null
    var ipv4Address: String? = null
    var ipv6Address: String? = null

    var inAppInstanceId: String? = null

    constructor(
        language: String,
        country: String,
        manufacturer: String,
        appVersion: String,
        osVersion: String,
        versionSdk: String,
        device: String,
        model: String,
        product: String,
        innerMacAddress: String,
        outerMacAddress: String,
        ipv4Address: String,
        ipv6Address: String,
        inAppInstanceId: String?
    ) {
        this.language = language
        this.country = country
        this.osVersion = osVersion
        this.versionSdk = versionSdk
        this.appVersion = appVersion
        this.device = device
        this.model = model
        this.product = product
        this.manufacturer = manufacturer
        this.innerMacAddress = innerMacAddress
        this.outerMacAddress = outerMacAddress
        this.ipv4Address = ipv4Address
        this.ipv6Address = ipv6Address
        this.inAppInstanceId = inAppInstanceId
    }

    constructor()
}