package com.triPcups.android.basicapp.features.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.triPcups.android.basicapp.common.ProgressData
import com.triPcups.android.basicapp.local_db.AppSettings
import com.triPcups.android.basicapp.models.ErrorData
import com.triPcups.android.basicapp.models.ErrorEvent
import com.triPcups.android.basicapp.networking.ApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import javax.security.auth.callback.Callback

class MainViewModel(private val appSettings: AppSettings,
                    private val apiService: ApiService) : ViewModel() {

    val progressData = ProgressData()
    val errorEvent = MutableLiveData<ErrorEvent>()
    val versionJsonUrlData= MutableLiveData<String>()
    val errorData = MutableLiveData<ErrorData>()
    val objectToObserve = MutableLiveData<Any>()

    init {
        viewModelScope.launch {
            simulateFirstCall()
            checkForNewerVersions()
        }
    }

    private suspend fun checkForNewerVersions()  = withContext(Dispatchers.IO) {
        /// TODO :: load from remote config json URL Object
        versionJsonUrlData.postValue("")
    }

    private suspend fun simulateFirstCall() = withContext(Dispatchers.IO) {
        progressData.startProgress()

        apiService.getUsers().enqueue(object : Callback,
            retrofit2.Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                progressData.endProgress()
                if (response.isSuccessful) {
                    objectToObserve.postValue(response as Any)
                } else {
                    errorEvent.postValue(ErrorEvent.UNSUCCESSFUL_RESPONSE)
                    errorData.postValue(ErrorData.RssFeedNotLoaded(response.message()))

                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                errorEvent.postValue(ErrorEvent.FAILURE)
                progressData.endProgress()
            }
        })
    }

    fun postFeedback(feedback: String?, rate: Float) {
        TODO("Not yet implemented")
    }

    fun logUiEvent(s: String, s1: String) {
        TODO("Not yet implemented")
    }

    fun logException(e: Exception, b: Boolean) {
        TODO("Not yet implemented")
    }

}
