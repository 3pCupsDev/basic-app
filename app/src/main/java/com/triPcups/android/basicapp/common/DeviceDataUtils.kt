package com.triPcups.android.basicapp.common

import android.os.Build
import com.triPcups.android.basicapp.models.DeviceDetails
import java.net.NetworkInterface
import java.util.*

object DeviceDataUtils {

    var instanceId: String? = null
        set(value) {
            field = value
        }

    fun constructDeviceDetails(locale: Locale): DeviceDetails {
        val manufacturer = Build.MANUFACTURER
        val model = Build.MODEL
        val device = Build.DEVICE
        val product = Build.PRODUCT
        val sdkVersion = Build.VERSION.BASE_OS + Build.VERSION.SDK_INT
        val appVersion = Build.VERSION.RELEASE

        val language = locale.language
        val country = locale.country
        val osVersion = Build.VERSION.BASE_OS

        val outerMac = getMACAddress("wlan0")
        val innerMac = getMACAddress("eth0")
        val ipv4Address = getIPAddress(true) // IPv4
        val ipv6Address = getIPAddress(false) // IPv6

        return DeviceDetails(
            language,
            country,
            manufacturer,
            appVersion,
            osVersion,
            sdkVersion,
            device,
            model,
            product,
            innerMac,
            outerMac,
            ipv4Address,
            ipv6Address,
            instanceId
        )
    }

    /**
     * Returns MAC address of the given interface name.
     * @param interfaceName eth0, wlan0 or NULL=use first interface
     * @return  mac address or empty string
     */
    fun getMACAddress(interfaceName: String?): String {
        try {
            val interfaces = Collections.list(NetworkInterface.getNetworkInterfaces())
            for (intf in interfaces) {
                if (interfaceName != null) {
                    if (!intf.name.equals(interfaceName, ignoreCase = true)) continue
                }
                val mac = intf.hardwareAddress ?: return ""
                val buf = StringBuilder()
                for (aMac in mac) buf.append(String.format("%02X:", aMac))
                if (buf.length > 0) buf.deleteCharAt(buf.length - 1)
                return buf.toString()
            }
        } catch (ignored: Exception) {
        }
        // for now eat exceptions
        return ""
        /*try {
            // this is so Linux hack
            return loadFileAsString("/sys/class/net/" +interfaceName + "/address").toUpperCase().trim();
        } catch (IOException ex) {
            return null;
        }*/
    }

    /**
     * Get IP address from first non-localhost interface
     * @param useIPv4   true=return ipv4, false=return ipv6
     * @return  address or empty string
     */
    fun getIPAddress(useIPv4: Boolean): String {
        try {
            val interfaces = Collections.list(NetworkInterface.getNetworkInterfaces())
            for (intf in interfaces) {
                val addrs = Collections.list(intf.inetAddresses)
                for (addr in addrs) {
                    if (!addr.isLoopbackAddress) {
                        val sAddr = addr.hostAddress
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        val isIPv4 = sAddr.indexOf(':') < 0

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr
                        } else {
                            if (!isIPv4) {
                                val delim = sAddr.indexOf('%') // drop ip6 zone suffix
                                return if (delim < 0) sAddr.toUpperCase() else sAddr.substring(
                                    0,
                                    delim
                                ).toUpperCase()
                            }
                        }
                    }
                }
            }
        } catch (ignored: Exception) {
        }
        // for now eat exceptions
        return ""
    }

}