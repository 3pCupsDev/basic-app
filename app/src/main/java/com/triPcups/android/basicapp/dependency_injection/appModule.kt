package com.triPcups.android.basicapp.dependency_injection

import com.triPcups.android.basicapp.features.main.MainViewModel
import com.triPcups.android.basicapp.features.splash.on_boarding.OnBoardingViewModel
import com.triPcups.android.basicapp.features.splash.splash_logo.SplashLogoViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    //Splash Screen
    viewModel { SplashLogoViewModel(get()) }

    //On Boarding Screen
    viewModel { OnBoardingViewModel(get()) }

    //Main Screen
    viewModel { MainViewModel(get(), get()) }
}