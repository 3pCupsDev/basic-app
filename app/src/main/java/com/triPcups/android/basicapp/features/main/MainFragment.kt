package com.triPcups.android.basicapp.features.main

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.codemybrainsout.ratingdialog.RatingDialog
import com.eggheadgames.aboutbox.AboutConfig
import com.eggheadgames.aboutbox.IAnalytic
import com.eggheadgames.aboutbox.IDialog
import com.triPcups.android.basicapp.R
import com.triPcups.android.basicapp.common.VersionUpdater
import com.triPcups.android.basicapp.local_db.Constants
import com.triPcups.android.basicapp.models.ErrorData
import com.triPcups.android.basicapp.models.ErrorEvent
import com.triPcups.android.basicapp.models.MessageType
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : Fragment() {

    interface MainFragmentListener{
        fun onLoading(isLoading: Boolean)
        fun showMessage(msg: String?, messageType: MessageType)
        fun openLink(url: String)
        fun showAboutActivity()
    }

    companion object {
        fun newInstance() = MainFragment()
    }

    private var listener: MainFragmentListener? = null
    private val viewModel by viewModel<MainViewModel>()
    private var rateUsDialog: RatingDialog? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainFragmentListener) {
            this.listener = context
        } else {
            throw RuntimeException("$context must implement MainFragmentListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRateUsDialog()
        initAboutUsActivity()
        initObservers()
        initUi()
    }

    private fun initObservers() {
        viewModel.progressData.observe(viewLifecycleOwner, Observer {
            isLoading -> handleLoading(isLoading) })
        viewModel.errorEvent.observe(viewLifecycleOwner, Observer {
                errorEvent -> handleErrors(errorEvent) })
        viewModel.errorData.observe(viewLifecycleOwner, Observer {
                errorDataMsg -> handleErrorMessages(errorDataMsg) })
        viewModel.versionJsonUrlData.observe(viewLifecycleOwner, Observer {
                jsonUrl -> handleVersionValidation(jsonUrl) })
//        viewModel.objectToObserve.observe(viewLifecycleOwner, Observer {
//                remoteConfigData -> handleRemoteConfig(remoteConfigData) })
    }

    private fun handleVersionValidation(jsonUrl: String?) {
        if(!jsonUrl.isNullOrEmpty()){
            VersionUpdater.validateVersion(context,jsonUrl)
        }
    }

    private fun initAboutUsActivity() {
        //todo change details of develoepr
        val aboutConfig = AboutConfig.getInstance()
        aboutConfig.apply {
            context?.let{
                version = VersionUpdater.getBuildVersion(it)
            }
            appName = getString(R.string.app_name)
            appIcon = R.mipmap.ic_launcher
            author = "3P Cups"
//            aboutLabelTitle="?whatt"
//            extraTitle = "extra"
//            sharingTitle = "sharing"
            aboutLabelTitle = "About The Developer"
            shareMessage= Constants.ABOUT_CONFIG_SHARE_ACTION_TEMPLATE
            packageName = context?.applicationContext?.packageName
            buildType = AboutConfig.BuildType.GOOGLE
            facebookUserName = "whatever"
            webHomePage = Constants.APP_WEB_HOME_PAGE_URL

            // app publisher for "Try Other Apps" item
            appPublisher = "3P Cups"

            // if pages are stored locally, then you need to override aboutConfig.dialog to be able use custom WebView
            companyHtmlPath = Constants.APP_WEB_HOME_PAGE_URL+"/p/about-us.html"
            privacyHtmlPath = Constants.APP_WEB_HOME_PAGE_URL + "/p/privacy-policy.html"

            dialog = IDialog { appCompatActivity, url, tag ->
                // handle custom implementations of WebView. It will be called when user click to web items. (Example: "Privacy", "Acknowledgments" and "About")
                listener?.openLink(url)
            }

//            analytics = object : IAnalytic {
//                override fun logUiEvent(s: String, s1: String) {
//                    // handle log events.
//                    viewModel.logUiEvent(s,s1)
//                }
//
//                override fun logException(e: Exception, b: Boolean) {
//                    // handle exception events.
//                    viewModel.logException(e,b)
//
//                }
//            }
//            // set it only if aboutConfig.analytics is defined.
//            logUiEventName = "Log"

            // Contact Support email details
            emailAddress = Constants.ABOUT_CONFIG_DEVELOPER_EMAIL_ADDRESS
            emailSubject = Constants.ABOUT_CONFIG_EMAIL_SUBJECT_TEMPLATE + getString(R.string.app_name)
            emailBody = Constants.ABOUT_CONFIG_EMAIL_BODY_TEMPLATE

        }
    }

    private fun initRateUsDialog() {
        var rate = 0f
        rateUsDialog = RatingDialog.Builder(context)
            .threshold(Constants.RATE_US_DIALOG_RATING_THRESHOLD)
            .session(Constants.RATE_US_DIALOG_MIN_SESSION_TO_APPEAR)
            .onRatingChanged { rating, thresholdCleared ->
                rate = rating
            }
            .onRatingBarFormSumbit { feedback ->
                postFeedback(feedback, rate)
            }.build()

        rateUsDialog?.show()
    }

    private fun postFeedback(feedback: String?, rate: Float) {
        viewModel.postFeedback(feedback,rate)
    }


    private fun handleErrorMessages(errorDataMsg: ErrorData?) {
        errorDataMsg?.let{
            when(it) {
                is ErrorData.RssFeedNotLoaded -> {
                    listener?.showMessage(it.msg, MessageType.FAILURE)
                }
            }
        }
    }

    private fun handleErrors(errorEvent: ErrorEvent?) {
        errorEvent?.let{
            when(it) {
                ErrorEvent.FAILURE -> {
                    listener?.showMessage(it.name, MessageType.FAILURE)
                }
                ErrorEvent.UNSUCCESSFUL_RESPONSE -> {
                    listener?.showMessage(it.name, MessageType.FAILURE)
                }
                ErrorEvent.NETWORK_ERROR -> {
                    listener?.showMessage(it.name, MessageType.CAUTION)
                }
                ErrorEvent.NO_ERROR -> {}
            }
        }
    }

    private fun handleLoading(loading: Boolean?) {
        loading?.let{
            listener?.onLoading(it)
            if(it){
                //show shimmer effect
            } else {
                //hide shimmer effect
            }
        }
    }

    private fun initUi() {
        //if a function stays empty we should delete it
    }

}
