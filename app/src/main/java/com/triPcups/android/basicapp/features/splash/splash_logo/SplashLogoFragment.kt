package com.triPcups.android.basicapp.features.splash.splash_logo

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.triPcups.android.basicapp.R
import com.triPcups.android.basicapp.models.ErrorEvent
import com.triPcups.android.basicapp.models.MessageType
import com.triPcups.android.basicapp.models.SplashNavigationEvent
import kotlinx.android.synthetic.main.splash_logo_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class SplashLogoFragment : Fragment() {

    interface SplashLogoFragmentListener{
        fun startNextActivity()
        fun onLoading(isLoading: Boolean)
        fun showFirstTimeInAppDialog()
        fun showMessage(msg: String?, messageType: MessageType)
        fun showSignInDialog()
    }

    companion object {
        fun newInstance() = SplashLogoFragment()
    }

    private var listener: SplashLogoFragmentListener? = null
    private val viewModel  by viewModel<SplashLogoViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.splash_logo_fragment, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is SplashLogoFragmentListener) {
            this.listener = context
        } else {
            throw RuntimeException("$context must implement SplashLogoFragmentListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initObservers()
        initUi()
    }

    private fun initObservers() {
        viewModel.progressData.observe(viewLifecycleOwner, Observer {
            isLoading -> handleLoading(isLoading) })
        viewModel.errorEvent.observe(viewLifecycleOwner, Observer {
            errorEvent -> handleErrors(errorEvent) })
        viewModel.appSettingsData.observe(viewLifecycleOwner, Observer {
            appSettingsData -> handleSettingsLoaded(appSettingsData) })
        viewModel.navigationEvent.observe(viewLifecycleOwner, Observer {
            navEvent -> handleNavEvents(navEvent) })
    }

    private fun handleNavEvents(navEvent: SplashNavigationEvent) {
        when(navEvent) {
            is SplashNavigationEvent.UserNeedSignedIn -> {
                listener?.showSignInDialog()
            }
            is SplashNavigationEvent.ShowOnBoarding -> {
                listener?.showFirstTimeInAppDialog()
            }
            is SplashNavigationEvent.LoadHome -> {
                listener?.startNextActivity()
            }
        }
    }

    private fun handleErrors(errorEvent: ErrorEvent?) {
        errorEvent?.let{
            when(it){
                ErrorEvent.NETWORK_ERROR-> {
                    listener?.showMessage("Network Error Message", MessageType.FAILURE)
                }
                ErrorEvent.UNSUCCESSFUL_RESPONSE -> {
                    listener?.showMessage("Unsuccessful Response Message", MessageType.FAILURE)
                }

                ErrorEvent.NO_ERROR -> {}
                else -> listener?.showMessage(errorEvent.name, MessageType.FAILURE)
            }
        }
    }

    private fun handleLoading(loading: Boolean?) {
        loading?.let{
            listener?.onLoading(it)
        }
    }

    private fun handleSettingsLoaded(appSettingsData: ArrayList<Object>?) {
        if (!appSettingsData.isNullOrEmpty()) {
            // do Something
            listener?.showMessage("Remote config settings loaded", MessageType.SUCCESS)

        }
    }

    private fun initUi() {
        val topAnim = AnimationUtils.loadAnimation(context,R.anim.top_animation)
        val bottomAnim = AnimationUtils.loadAnimation(context,R.anim.bottom_animation)

        splashActAppLogo.animation = topAnim
        splashActAppName.animation = topAnim
        splashActAppSlogan.animation = bottomAnim
    }

}
