package com.triPcups.android.basicapp.features.splash.splash_logo

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.triPcups.android.basicapp.common.ProgressData
import com.triPcups.android.basicapp.local_db.AppSettings
import com.triPcups.android.basicapp.models.ErrorEvent
import com.triPcups.android.basicapp.models.SplashNavigationEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SplashLogoViewModel(private val appSettings: AppSettings) : ViewModel() {

    val progressData = ProgressData()
    val errorEvent = MutableLiveData<ErrorEvent>()
    val navigationEvent = MutableLiveData<SplashNavigationEvent>()
    val appSettingsData = MutableLiveData<ArrayList<Object>>()

    init{
        viewModelScope.launch{
            simulateAppSettingsCall()   // could also be for remote config fetching
            checkForFirstTime()
        }
    }

    private suspend fun checkForFirstTime() = withContext(Dispatchers.IO) {
        val firstTimeInApp = appSettings.getFirstTimeInApp()
        val isSignedIn = appSettings.getIsSignedIn()
        val dontWantSignedIn = appSettings.getDontWantSignedIn()

        if(firstTimeInApp){
            navigationEvent.postValue(SplashNavigationEvent.ShowOnBoarding())
        } else if (!isSignedIn) {
            if(dontWantSignedIn){
                navigationEvent.postValue(SplashNavigationEvent.LoadHome())
            } else {
                navigationEvent.postValue(SplashNavigationEvent.UserNeedSignedIn())
            }
        } else {
            navigationEvent.postValue(SplashNavigationEvent.LoadHome())
        }
    }

    private suspend fun simulateAppSettingsCall() = withContext(Dispatchers.IO){
        progressData.startProgress()
        val modelList = appSettings.getModelList()
        appSettingsData.postValue(modelList)
        progressData.endProgress()
    }

}
