package com.triPcups.android.basicapp.features

import android.net.Uri
import android.os.Bundle
import android.view.Gravity
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import androidx.core.text.TextUtilsCompat
import androidx.core.view.GravityCompat
import androidx.core.view.ViewCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.codemybrainsout.ratingdialog.RatingDialog
import com.eggheadgames.aboutbox.AboutConfig
import com.eggheadgames.aboutbox.IAnalytic
import com.eggheadgames.aboutbox.IDialog
import com.eggheadgames.aboutbox.activity.AboutActivity
import com.emmanuelkehinde.shutdown.Shutdown
import com.prathameshmore.toastylibrary.Toasty
import com.triPcups.android.basicapp.R
import com.triPcups.android.basicapp.common.AndroidBarsManager
import com.triPcups.android.basicapp.common.VersionUpdater
import com.triPcups.android.basicapp.custom_views.BottomBar
import com.triPcups.android.basicapp.custom_views.HeaderView
import com.triPcups.android.basicapp.features.main.MainFragment
import com.triPcups.android.basicapp.local_db.Constants
import com.triPcups.android.basicapp.models.MessageType
import com.yariksoffice.lingver.Lingver
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_drawer.*
import saschpe.android.customtabs.CustomTabsHelper
import saschpe.android.customtabs.WebViewFallback


class MainActivity : AppCompatActivity(), HeaderView.HeaderViewListener,
    MainFragment.MainFragmentListener, BottomBar.BottomBarListener {

    private lateinit var toasty: Toasty
    private var languager: Lingver? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        toasty = Toasty(this@MainActivity)
        languager = Lingver.getInstance()

        initDrawer()
        initHeaderView()
        initBottomBar()
        initUi()
    }

    private fun initBottomBar() {
        mainActBottomBar.listener = this
    }

    private fun initDrawer() {
        drawerLayout.addDrawerListener(object : DrawerLayout.DrawerListener{

            override fun onDrawerStateChanged(newState: Int) {

            }

            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                val isLeftToRight = TextUtilsCompat.getLayoutDirectionFromLocale(languager?.getLocale()) == ViewCompat.LAYOUT_DIRECTION_LTR

                if(isLeftToRight){
                    coordinatorLayout.translationX = slideOffset * drawerView.width
                } else {
                    coordinatorLayout.translationX = -slideOffset * drawerView.width
                }
                invalidateOptionsMenu()
            }

            override fun onDrawerClosed(drawerView: View) {
                mainActHeaderView.playMenuAnimation(false)
            }

            override fun onDrawerOpened(drawerView: View) {
                mainActHeaderView.playMenuAnimation(true)
            }
        })
    }

    private fun initUi() {
//        AndroidBarsManager.setColor(window,getColor(R.color.colorPrimaryDark))
        showMainFragment()
    }

    private fun showMainFragment(){
        showFragment(MainFragment.newInstance(), Constants.MAIN_FRAGMENT_TAG)
    }

    private fun initHeaderView() {
        mainActHeaderView.listener = this

        mainActHeaderView.setType(HeaderView.HEADER_VIEW_TYPE_TITLE_MENU_SEARCH,getString(R.string.app_name))
//        onLoading(false) //TODO:: change this
    }

    override fun onLoading(isLoading: Boolean){
        mainActPb.showProgressBar(isLoading)
    }

    override fun onHeaderMenuClick() {
//        mainActHeaderView.playMenuAnimation(true)
        drawerLayout.openDrawer(GravityCompat.START,true)
    }

    override fun onHeaderSearchClick(screenName: String) {
        showAboutActivity()
    }

    override fun onBottomCartClick() {
        mainActHeaderView.setTitle(getString(R.string.cart))
    }

    override fun onBottomHomeClick() {
        mainActHeaderView.setTitle(getString(R.string.home))
        showMainFragment()
    }

    override fun onBottomMenuClick() {
        mainActHeaderView.setTitle(getString(R.string.menu))
    }

    override fun onBottomProfileClick() {
        mainActHeaderView.setTitle(getString(R.string.profile))
    }

    override fun onBottomUpdateClick() {
        mainActHeaderView.setTitle(getString(R.string.updates))
    }

    override fun openLink(url: String){
        val customTabsIntent = CustomTabsIntent.Builder()
//            .addMenuItem("nana",)
//            .setCloseButtonIcon(ContextCompat.getDrawable(this,R.drawable.ic_close))
            .setToolbarColor(ContextCompat.getColor(this,R.color.colorPrimaryDark))
            .setShowTitle(true)
            .addDefaultShareMenuItem()
            .build()

        // This is optional but recommended
        CustomTabsHelper.addKeepAliveExtra(this, customTabsIntent.intent)

        // This is where the magic happens...
        CustomTabsHelper.openCustomTab(this, customTabsIntent,
            Uri.parse(url),
            WebViewFallback()
        )
    }

    override fun showAboutActivity(){
        AboutActivity.launch(this)
//        overridePendingTransition(R.anim.anim_in,R.anim.anim_out)
    }

    override fun showMessage(msg: String?, messageType: MessageType) {
        msg?.let{
            when(messageType) {
                MessageType.FAILURE ->{
                    toasty.dangerToasty(
                        this,
                        msg,
                        Toasty.LENGTH_LONG,
                        Toasty.BOTTOM
                    )
                }
                MessageType.CAUTION ->{
                    toasty.warningToasty(
                        this,
                        msg,
                        Toasty.LENGTH_LONG,
                        Toasty.BOTTOM
                    )
                }
                MessageType.SUCCESS ->{
                    toasty.successToasty(
                        this,
                        msg,
                        Toasty.LENGTH_LONG,
                        Toasty.BOTTOM
                    )
                }
                MessageType.NEAUTRAL ->{
                    toasty.infoToasty(
                        this,
                        msg,
                        Toasty.LENGTH_LONG,
                        Toasty.BOTTOM
                    )
                }
            }
        }
    }

    private fun showFragment(fragment: Fragment, tag: String) {
        supportFragmentManager.beginTransaction()
            .setCustomAnimations(
                R.anim.anim_in, 0, 0,
                R.anim.anim_out
            )
            .replace(R.id.mainActContainer, fragment,tag)
            .commit()
    }

    override fun onBackPressed() {
        Shutdown.now(this)
    }
}
